from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from django.db import models


class Session(models.Model):
    # Fields
    secret=models.CharField(
        max_length=64,
        blank=True,
        unique=True,
    )
    nonce=models.PositiveIntegerField(
        blank=True,
        unique=True,
        validators=[MaxValueValidator(2147483647)],
    )
    user=models.ForeignKey(User)
    data=models.TextField(
        blank=True,
        default="[]",
    )
    created_at=models.DateTimeField(
        auto_now_add=True,
        blank=True,
    )
    updated_at=models.DateTimeField(
        auto_now=True,
        blank=True,
    )
