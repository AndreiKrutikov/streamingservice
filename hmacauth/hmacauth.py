from datetime import datetime, timedelta, timezone
from dateutil.parser import parse

from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User, AnonymousUser
from django.db.utils import IntegrityError
from django.utils.deprecation import MiddlewareMixin

import hashlib, hmac
from .models import Session
from random import choice, randint

from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer

from string import ascii_letters, digits

class IncorrectPassword(Exception):
    def __init__(self, username):
        super().__init__("Incorrect password for user '{0}'".format(username))

class WrappedResponse(Response):
    def __init__(self, data=None, status=None, template_name=None,
        headers=None, exception=False, content_type=None, errors=[]):
        newdata={
            'success': not bool(errors),
            'errors': errors,
        }
        if data:
            newdata['data']=data
        super().__init__(data=newdata, status=status,
            template_name=template_name, headers=headers,
            exception=exception, content_type=content_type)
        self.success=not bool(errors)

def permission_required(right):
    def wrapper(func):
        def access_denied_func(*args, **kwargs):
            if hasattr(args[0], 'session') and args[0].session.user.has_perm(right):
                return func(*args, **kwargs)
            response=WrappedResponse(errors=['Access denied'])
            response.accepted_renderer=JSONRenderer()
            response.accepted_media_type=response.accepted_renderer.media_type
            response.renderer_context={}
            return response
        return access_denied_func
    return wrapper    

def login_required(func):
    def is_auth(*args, **kwargs):
        if hasattr(args[0], 'session'):
            return func(*args, **kwargs) 
        response=WrappedResponse(errors=['Access denied'])
        response.accepted_renderer=JSONRenderer()
        response.accepted_media_type=response.accepted_renderer.media_type
        response.renderer_context={}
        return response
    return is_auth

class HMACMiddleware(MiddlewareMixin):
    def process_request(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)
        if set(request.META.keys()).issuperset({'HTTP_SESSION_ID', 'HTTP_TIMESTAMP', 'HTTP_SIGNATURE'}):
            try:
                session=Session.objects.get(
                    id=request.META['HTTP_SESSION_ID'],
                )
            except (Session.DoesNotExist, ValueError):
                response=WrappedResponse(
                    errors=["Doesn't exist session with '{0}' id".format(request.META['HTTP_SESSION_ID'])],
                )
                response.accepted_renderer=JSONRenderer()
                response.accepted_media_type=response.accepted_renderer.media_type
                response.renderer_context={}
                return response
            try:
                timestamp=parse(request.META['HTTP_TIMESTAMP'])
            except ValueError:
                response=WrappedResponse(
                    errors=["Incorrect timestamp format"],
                )
                response.accepted_renderer=JSONRenderer()
                response.accepted_media_type=response.accepted_renderer.media_type
                response.renderer_context={}
                return response
            delta=timedelta(minutes=30)
            now=datetime.now(timezone.utc)
            if now-delta>timestamp or now+delta<timestamp:
                response=WrappedResponse(errors=["Incorrect timestamp"])
                response.accepted_renderer=JSONRenderer()
                response.accepted_media_type=response.accepted_renderer.media_type
                response.renderer_context={}
                return response
            sha=hmac.new(
                str(session.secret).encode(),
                (request.get_full_path()+request.META['HTTP_TIMESTAMP']+str(session.nonce)+str(session.id)+request.body.decode('utf-8')).encode(),
                digestmod=hashlib.sha256,
            ).hexdigest()
            print((request.get_full_path()+request.META['HTTP_TIMESTAMP']+str(session.nonce)+str(session.id)+request.body.decode('utf-8')).encode())
            print(sha)
            if sha!=request.META['HTTP_SIGNATURE']:
                response=WrappedResponse(errors=["Incorrect signature"])
                response.accepted_renderer=JSONRenderer()
                response.accepted_media_type=response.accepted_renderer.media_type
                response.renderer_context={}
                return response
            request.session=session
    
    def process_response(self, request, response):
        print(request.path)
        if hasattr(response, 'success') and response.success and hasattr(request, 'session'):
            while True:
                try:
                    request.session.nonce=randint(0, 2147483647)
                    request.session.save()
                    break
                except IntegrityError:
                    pass
            response["Nonce"]=request.session.nonce
            print(request.session.user)
        response['Access-Control-Expose-Headers']="Content-Type, Session-ID, Nonce"
        response['Access-Control-Allow-Origin']='*'
        response['Access-Control-Allow-Headers']="Timestamp, Content-Type, Session-ID, Signature"
        response['Access-Control-Allow-Methods']="GET, POST, PUT, OPTIONS, DELETE, HEAD"
        return response
                    
class AuthenticationBackend:
    def authenticate(username, password):
        try:
            user=User.objects.get(username=username)
        except User.DoesNotExist:
            user=User.objects.get(email=username)
        if user.check_password(password):
            return user
        else:
            raise IncorrectPassword(username)
    
    def get_user(self, id):
        try:
            return User.objects.get(pk=id)
        except User.DoesNotExist:
            return None
        
    def login(request, user):
        if hasattr(request, 'session'):
            self.logout(request)
        sessions=Session.objects.filter(user=user)
        if sessions.count()==user.userdata.sessions_count:
            sessions=sessions.order_by('updated_at')
            sessions[0].delete()
        while True:
            try:
                session=Session(
                    secret=''.join(choice(ascii_letters+digits) for i in range(64)),
                    nonce=randint(0, 2147483647),
                    user=user,
                )
                session.save()
                request.session=session
                break
            except ValueError:
                pass
        
    def logout(request):
        if hasattr(request, 'session'):
            request.session.delete()
            delattr(request, 'session')
