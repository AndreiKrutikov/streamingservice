from django.contrib.auth.models import User, Group

from mainapp.models import *

from rest_framework import serializers

class PartSerializer(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        remove_fields = kwargs.pop('remove_fields', None)
        super(PartSerializer, self).__init__(*args, **kwargs)
        
        if remove_fields:
            for field_name in remove_fields:
                if field_name in self.fields:
                    self.fields.pop(field_name)



class GroupSerializer(PartSerializer, serializers.ModelSerializer):
    class Meta:
        model=Group
        fields=('name', )

class TagSerializer(PartSerializer, serializers.ModelSerializer):
    class Meta:
        model=Tag
        fields=('name', 'group', 'created_at', 'updated_at')

class TagGroupSerializer(PartSerializer, serializers.ModelSerializer):
    class Meta:
        model=TagGroup
        fields=('name', 'color', 'created_at', 'updated_at')
        extra_kwargs={
            'color': {'required': False},
        }

class SongSerializer(PartSerializer, serializers.ModelSerializer):
    path=serializers.CharField(
        read_only=True,
        required=False,
    )
    cover_path=serializers.CharField(
        read_only=True,
        required=False,
    )
    
    class Meta:
        model=Song
        fields=('id', 'title', 'artist', 'album', 'release_year',
            'duration', 'file_size','tags', 'path', 'cover_path', 'author',
            'created_at', 'updated_at')
        extra_kwargs = {
            'title': {'required': False},
            'artist': {'required': False},
            'album': {'required': False},
            'release_year': {'required': False},
            'tags': {'required': False},
        }

class JingleSerializer(PartSerializer, serializers.ModelSerializer):
    class Meta:
        model=Jingle
        fields=('id', 'title', 'author', 'path', 'duration',
            'created_at', 'updated_at')

class PlaylistSerializer(PartSerializer, serializers.ModelSerializer):
    class Meta:
        model=Playlist
        fields=('id', 'name', 'tags', 'created_at', 'updated_at')
        extra_kwargs={
            'tags': {'required': False},
        }
        depth=0

class UserdataSerializer(PartSerializer, serializers.ModelSerializer): 
    playlist=PlaylistSerializer()
    
    class Meta:
        model=Userdata
        fields=('playlist', 'sessions_count')

class UserSerializer(PartSerializer, serializers.ModelSerializer):
    groups=GroupSerializer(many=True)
    userdata=UserdataSerializer()
    
    class Meta:
        model=User
        fields=('id', 'username', 'email', 'groups', 'userdata',
            'is_superuser')
        depth=1

class FeedbackSerializer(PartSerializer, serializers.ModelSerializer):
    class Meta:
        model=Feedback
        fields=('id', 'playlist', 'user', 'song', 'mark', 'created_at',
            'updated_at')

class CallbackSerializer(PartSerializer, serializers.ModelSerializer):
    class Meta:
        model=Callback
        fields=('id', 'phone', 'email', 'user', 'message', 'close',
            'created_at', 'updated_at')
        extra_kwargs={
            'phone': {'required': False},
            'email': {'required': False},
            'user': {'required': False},
        }
