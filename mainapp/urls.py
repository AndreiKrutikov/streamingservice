from django.conf.urls import url

from mainapp import views, api

urlpatterns=[
    # WEB urls
    url(r'^$', views.TestView.as_view(), name='web_test'),

    # REST API
    url(r'^api/login/?$', api.LoginView.as_view(), name='api_login'),
    url(r'^api/logout/?$', api.LogoutView.as_view(), name='api_logout'),
    url(r'^api/register/?$', api.RegisterView.as_view(), name='api_register'),
    
    url(r'^api/users/?$', api.UsersView.as_view(), name='api_users'),
    url(r'^api/songs/?$', api.SongsView.as_view(), name='api_songs'),
    url(r'^api/jingles/?$', api.JinglesView.as_view(), name='api_jingles'),
    url(r'^api/tags/?$', api.TagsView.as_view(), name='api_tags'),
    url(r'^api/taggroups/?$', api.TagGroupsView.as_view(), name='api_taggroups'),
    url(r'^api/playlists/?$', api.PlaylistsView.as_view(), name='api_playlists'),
    url(r'^api/feedbacks/?$', api.FeedbacksView.as_view(), name='api_feedbacks'),
    url(r'^api/callbacks/?$', api.CallbacksView.as_view(), name='api_callbacks'),
    url(r'^api/shuffle/?$', api.ShuffleView.as_view(), name='api_shuffle'),
    
    url(r'^api/songs/update/?$', api.SongsUpdateView.as_view(), name='api_songs_update'),
    url(r'^api/jingles/update/?$', api.JinglesUpdateView.as_view(), name='api_jingles_update'),
    
    url(r'^api/users/([0-9]+)/?$', api.UserView.as_view(), name='api_user'),
    url(r'^api/songs/([0-9]+)/?$', api.SongView.as_view(), name='api_song'),
    url(r'^api/jingles/([0-9]+)/?$', api.JingleView.as_view(), name='api_jingle'),
    url(r'^api/tags/(\w+)/?$', api.TagView.as_view(), name='api_tags'),
    url(r'^api/taggroups/(\w+)/?$', api.TagGroupView.as_view(), name='api_taggroup'),
    url(r'^api/playlists/([0-9]+)/?$', api.PlaylistView.as_view(), name='api_playlists'),
    url(r'^api/feedbacks/([0-9]+)/?$', api.FeedbackView.as_view(), name='api_feedbacks'),
    url(r'^api/callbacks/([0-9]+)/?$', api.CallbackView.as_view(), name='api_callbacks'),
    
    url(r'^api/songs/([0-9]+)/file/?$', api.SongFileView.as_view(), name='api_song_file'),
    url(r'^api/songs/([0-9]+)/played/?$', api.SongPlayedView.as_view(), name='api_song_played'),
    url(r'^api/jingles/([0-9]+)/file/?$', api.JingleFileView.as_view(), name='api_jingle_file'),
    
    # Media
    url(r'^media/songs/(.*)$', views.MediaSongView.as_view(), name='media_song'),
    url(r'^media/jingles/(.*)$', views.MediaJingleView.as_view(), name='media_jingle'),
]

