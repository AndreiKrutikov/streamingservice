from datetime import datetime
from dateutil.parser import parse

from django.contrib.auth.models import User, Group
from django.core.exceptions import FieldError
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from django.db.utils import DataError, IntegrityError
from django.utils.decorators import method_decorator
from django.urls import reverse

import json
from json.decoder import JSONDecodeError

from hashlib import md5
from hmacauth.hmacauth import WrappedResponse, AuthenticationBackend, IncorrectPassword, login_required, permission_required

from mainapp.auth import IncorrectPassword
from mainapp.serializer import *
from mainapp.models import *
from mainapp.pagination import CustomPagination

from mutagen import File

import os

from random import choice, randint

from rest_framework.generics import ListAPIView
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from streamingservice.settings import MEDIA_ROOT
from string import ascii_letters, digits

paginator=CustomPagination()
paginator.default_limit=100
paginator.max_limit=100000


class LoginView(APIView): # Ready
    def post(self, request):
        errors=[]
        if not 'username' in request.data:
            errors.append("Username required")
        if not 'password' in request.data:
            errors.append("Password required")
        if errors:
            return WrappedResponse(errors=errors)
        try:
            user=AuthenticationBackend.authenticate(
                username=request.data['username'],
                password=request.data['password'],
            )
        except (User.DoesNotExist, IncorrectPassword) as e:
            errors.append("Incorrect password or email/username")
        if errors:
            return WrappedResponse(errors=errors)
        AuthenticationBackend.login(request._request, user)
        response=WrappedResponse(
            {'secret': request.session.secret},
        )
        response['Session-ID']=request.session.id
        response['Nonce']=request.session.nonce
        return response
        
class LogoutView(APIView): # Ready
    @method_decorator(login_required)
    def post(self, request):
        AuthenticationBackend.logout(request._request)
        return WrappedResponse()

class RegisterView(APIView): # Ready
    def post(self, request):
        errors=[]
        if not 'username' in request.data:
            errors.append("Username required")
        if not 'email' in request.data:
            errors.append("Email required")
        if not 'password' in request.data:
            errors.append("Password required")
        if 'group' in request.data:
            if request.data['group']!='user':
                if hasattr(request, 'session') and request.session.user.is_superuser:
                    try:
                        group=Group.objects.get(name=request.data['group'])
                    except Group.DoesNotExist:
                        errors.append("Doesn't exist '{0}' group".format(request.data['group']))
                else:
                    errors.append("Access denied for set group")
            else:
                group=Group.objects.get(name='user')
        else:
            group=Group.objects.get(name='user')
        if 'sessions_count' in request.data:
            if hasattr(request, 'session') and request.session.user.has_perm('auth.change_user'):
                sessions_count=request.data['sessions_count']
            else:
                sessions_count=1
                errors.append("Access denied for set sessions count")
        else:
            sessions_count=1
        if 'playlist' in request.data:
            if hasattr(request, 'session') and request.session.user.has_perm('auth.change_user'):
                try:
                    playlist=Playlist.objects.get(name=request.data['playlist'])
                except Playlist.DoesNotExist:
                    errors.append("Doesn't exist '{0}' playlist".format(request.data['playlist']))
            else:
                errors.append("Access denied for set playlist")
        else:
            playlist=None
        if errors:
            return WrappedResponse(errors=errors)
        try:
            user=User.objects.create_user(
                username=request.data['username'],
                email=request.data['email'],
                password=request.data['password'],
            )
            user.groups.add(group)
            user.save()
            userdata=Userdata(user=user)
            userdata.sessions_count=sessions_count
            userdata.playlist=playlist
            userdata.save()
        except IntegrityError:
            return WrappedResponse(
                errors=["User with this data already exists"],
            )
        AuthenticationBackend.login(request._request, user)
        response=WrappedResponse({'secret': request.session.secret})
        response['Session-ID']=request.session.id
        response['Nonce']=request.session.nonce
        return response

class UsersView(APIView): # Ready
    @method_decorator(permission_required('auth.see_user'))
    def get(self, request): 
        users=User.objects.all()
        value=request.query_params.get('username', None)
        if value:
            users=users.filter(username__regex=value)
        value=request.query_params.get('email', None)
        if value:
            users=users.filter(email__regex=value)
        value=request.query_params.get('group', None)
        if value:
            if value=='admin':
                users=users.filter(is_superuser=True)
            else:
                users=users.filter(groups__name=value)
        value=request.query_params.get('playlist', None)
        if value:
            users=users.filter(userdata__playlist__name=value)
        value=request.query_params.get('sessions_count', None)
        if value:
            users=users.filter(userdata__sessions_count=value)
        value=request.query_params.get('order_by', None)
        if value:
            users=users.order_by(value)
        try:
            page=paginator.paginate_queryset(users, request)
        except (DataError, FieldError):
            return WrappedResponse(errors=["Incorrect search data"])
        users=UserSerializer(
            page,
            many=True,
        )
        data=paginator.get_paginated_response(users.data).data
        for user in data['results']:
            user.update(user['userdata'])
            del user['userdata']
            if user['is_superuser']:
                user['group']='admin'
            elif user['groups']:
                user['group']=user['groups'][0]['name']
            else:
                user['group']='guest'
            del user['is_superuser']
            del user['groups']
        return WrappedResponse(data)

class UserView(APIView): # Ready
    @method_decorator(permission_required('auth.see_user'))
    def get(self, request, id): # Ready
        try:
            user=User.objects.get(pk=id)
        except User.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist user with {0} id".format(id)],
            )
    
        user=UserSerializer(user)
        user=user.data
        user.update(user['userdata'])
        del user['userdata']
        if user['is_superuser']:
            user['group']='admin'
        elif user['groups']:
            user['group']=user['groups'][0]['name']
        else:
            user['group']='guest'
        del user['is_superuser']
        del user['groups']
        return WrappedResponse(user)
    
    @method_decorator(permission_required('auth.change_user'))
    def post(self, request, id): # Ready
        try:
            user=User.objects.get(pk=id)
        except User.DoesNotExists:
            return WrappedResponse(
                errors=["Doesn't exist user with {0} id".format(id)],
            )
        if 'username' in request.data and request.data['username']:
            user.username=request.data['username']
        if 'email' in request.data and request.data['email']:
            user.email=request.data['email']
        if 'group' in request.data and request.data['group'] and request.session.user.is_superuser:
            try:
                group=Group.objects.get(name=request.data['group'])
            except Group.DoesNotExist:
                return WrappedResponse(
                    errors=["Incorrect {0} group".format(request.data['group'])],
                )
            user.groups.clear()
            user.groups.add(group)
        if 'playlist' in request.data and request.data['playlist']:
            try:
                playlist=Playlist.objects.get(name=request.data['playlist'])
            except:
                return WrappedResponse(
                    errors=["Incorrect {0} playlist".format(request.data['playlist'])],
                )
            user.userdata.playlist=playlist
        if 'sessions_count' in request.data and request.data['sessions_count']:
            try:
                user.userdata.sessions_count=request.data['sessions_count']
            except:
                return WrappedResponse(
                    errors=["Incorrect {0} sessions count".format(request.data['sessions_count'])],
                )
        try:
            user.save()
            user.userdata.save()
            return WrappedResponse()
        except:
            return WrappedResponse(errors=["Incorrect data"])
    
    @method_decorator(permission_required('auth.delete_user'))
    def delete(self, request, id):
        try:
            user=User.objects.get(pk=id)
        except User.DoesNotExists:
            return WrappedResponse(
                errors=["Doesn't exist user with {0} id".format(id)],
            )
        if not request.session.user.is_superuser and not user.groups.filter(name__in=['user']).exists():
            return WrappedResponse(errors=["Access denied"])
        user.delete()
        return WrappedResponse()

class SongsView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_song'))
    def get(self, request): # Ready
        songs=Song.objects.all()
        
        # Filter
        value=request.query_params.get('title', None)
        if value:
            songs=songs.filter(title__regex=value)
        value=request.query_params.get('artist', None)
        if value:
            songs=songs.filter(artist__regex=value)
        value=request.query_params.get('album', None)
        if value:
            songs=songs.filter(album__regex=value)
        value=request.query_params.get('release_year', None)
        compare=request.query_params.get('release_year_compare', '')
        if value and not value.isdigit():
            return WrappedResponse(errors=["Incorrect 'release_year'"])
        if not compare in ('', '__gt', '__lt', '__gte', '__lte'):
            return WrappedResponse(
                errors=["Incorrect 'release_year_compare'"],
            )
        if value:
            songs=songs.filter(**{'release_year'+compare: int(value)})
        value=request.query_params.get('duration', None)
        compare=request.query_params.get('duration_compare', '')
        if value and not value.isdigit():
            return WrappedResponse(errors=["Incorrect 'duration'"])
        if not compare in ('', '__gt', '__lt', '__gte', '__lte'):
            return WrappedResponse(
                errors=["Incorrect 'duration_compare'"],
            )
        if value:
            songs=songs.filter(**{'duration'+compare: value})
        try:
            tags=set(json.loads(request.query_params.get('tags', '[]')))
        except JSONDecodeError:
            return WrappedResponse(
                errors=["Incorrect tags"],
            )
        for tag in tags:
            try:
                Tag.objects.get(name=tag)
            except Tag.DoesNotExist:
                return WrappedResponse(
                    errors=["Doesn't exist tag '{0}'".format(tag)],
                )
        compare=request.query_params.get('tags_compare', 'in')
        ids=[]
        if not compare in ('eq', 'in', 'nin'):
            return WrappedResponse(
                errors=["Incorrect 'tags_compare'"],
            )
        if compare=='eq':
            for song in songs:
                if set(json.loads(song.tags))!=tags:
                    ids.append(song.id)
        else:
            for song in songs:
                if (tags.issubset(set(json.loads(song.tags))))==(compare=='nin'):
                    ids.append(song.id)
        for x in ids:
            songs=songs.exclude(id=x)
        order_by=request.query_params.get('order_by', None)
        if order_by:
            songs=songs.order_by(order_by)
        
        try:
            page=paginator.paginate_queryset(songs, request)
        except (DataError, FieldError):
            return WrappedResponse(errors=["Incorrect search data"])
        
        songs=SongSerializer(
            page,
            remove_fields=['path', 'cover_path'],
            many=True,
        )
        data=paginator.get_paginated_response(songs.data).data
        for song in data['results']:
            song['tags']=json.loads(song['tags'])
        return WrappedResponse(data)
    
    @method_decorator(permission_required('mainapp.add_song'))
    def post(self, request): # Ready
        data=request.data
        delete=[]
        for key, value in data.items():
            if not value:
                delete.append(key)
        for key in delete:
            del data[key]
        if 'tags' in data:
            for tag in data['tags']:
                try:
                    Tag.objects.get(name=tag)
                except Tag.DoesNotExist:
                    return WrappedResponse(
                        errors=["Doesn't exist tag '{0}'".format(tag)],
                    )
            data['tags']=json.dumps(data['tags'])
        data['author']=request.session.user.id
        song=SongSerializer(data=data)
        if song.is_valid():
            obj=song.save()
            while True:
                try:
                    obj.link=''.join(choice(ascii_letters+digits) for i in range(64))
                    break
                except ValueError:
                    pass
            obj.save()
            return WrappedResponse({
                'link': reverse('api_songs')+'?link='+obj.link,
                'id': obj.id,
            })
        return WrappedResponse(errors=song.errors)
    
    def put(self, request): # Ready
        if not 'link' in request.query_params:
            return WrappedResponse(errors=["Link required"])
        try:
            song=Song.objects.get(link=request.query_params['link'])
        except Song.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist song with {0} upload link".format(request.query_params['link'])]
            )
        if not 'file' in request.data:
            return WrappedResponse(errors=["No uploaded song"])
        songfile=request.data.get("file")
        if songfile.content_type[:5]!='audio':
            return WrappedResponse(errors=["Uploaded file isn't song"])
        sf=File(songfile.temporary_file_path(), easy=True)
        if not song.title and 'title' in sf.tags:
            song.title=sf.tags['title'][0]
        if not song.artist and 'artist' in sf.tags:
            song.artist=sf.tags['artist'][0]
        if not song.album and 'album' in sf.tags:
            song.album=sf.tags['album'][0]
        if not song.release_year and 'date' in sf.tags:
            year=sf.tags['date'][0]
            if year.isdigit():
                song.release_year=year
        sf.delete()
        song.duration=sf.info.length
        fs=FileSystemStorage(location=MEDIA_ROOT+"/songs")
        while True:
            songfilename=''.join(choice(ascii_letters+digits) for i in range(10))+'.'+songfile.content_type[6:]
            if not fs.exists(songfilename):
                break
        song.path=fs.save(songfilename, ContentFile(songfile.read()))
        song.file_size=songfile.size
        song.link=None
        song.save()
        return WrappedResponse()
    
class SongsUpdateView(APIView): # Ready
    @method_decorator(permission_required('mainapp.update_song'))
    def post(self, request):
        fs=FileSystemStorage(location=MEDIA_ROOT+"/songs")
        response={'added': 0, 'deleted': 0}
        for media in fs.listdir('.')[1]:
            try:
                song=Song.objects.get(path=media)
            except:
                sf=File(MEDIA_ROOT+'/songs/'+media, easy=True)
                if sf is None:
                    response['deleted']+=1
                    os.remove(MEDIA_ROOT+'/songs/'+media)
                    continue
                data={}
                if sf.tags:
                    if 'title' in sf.tags:
                        data['title']=sf.tags['title'][0]
                    if 'artist' in sf.tags:
                        data['artist']=sf.tags['artist'][0]
                    if 'album' in sf.tags:
                        data['album']=sf.tags['album'][0]
                    if 'date' in sf.tags:
                        data['release_year']=sf.tags['date'][0]
                sf.delete()
                data['duration']=sf.info.length
                sf.save()
                while True:
                    name=''.join(choice(ascii_letters+digits) for i in range(10))+os.path.splitext(media)[1]
                    if not fs.exists(name):
                        break
                os.rename(
                    MEDIA_ROOT+'/songs/'+media,
                    MEDIA_ROOT+'/songs/'+name,
                )
                response['added']+=1
                data['path']=name
                data['author']=request.session.user
                song=Song(**data)
                song.file_size=os.path.getsize(MEDIA_ROOT+'/songs/'+name)
                song.save()
        songs=Song.objects.all()
        for song in songs:
            if not fs.exists(song.path):
                response['deleted']+=1
                song.delete()
        return WrappedResponse(response)

class SongView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_song'))
    def get(self, request, id): # Ready
        try:
            song=Song.objects.get(pk=id)
        except Song.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist track with {0} id".format(id)],
            )
        song=SongSerializer(
            song, 
            remove_fields=['path', 'cover_path'],
        )
        data=song.data
        data['tags']=json.loads(data['tags'])
        return WrappedResponse(data)
    
    @method_decorator(permission_required('mainapp.change_song'))
    def post(self, request, id): # Ready
        try:
            song=Song.objects.get(pk=id)
        except Song.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist track with {0} id".format(id)],
            )
        data=request.data
        delete=[]
        for key, value in data.items():
            if not value:
                delete.append(key)
        for key in delete:
            del data[key]
        if 'tags' in data:
            for tag in data['tags']:
                try:
                    Tag.objects.get(name=tag)
                except Tag.DoesNotExist:
                    return WrappedResponse(
                        errors=["Doesn't exist tag '{0}'".format(tag)],
                    )
            data['tags']=json.dumps(data['tags'])
        song=SongSerializer(
            song,
            remove_fields=['id', 'duration', 'path', 'cover_path',
                'author', 'created_at', 'updated_at'],
            data=data,
        )
        if song.is_valid():
            song.save()
            return WrappedResponse()
        return WrappedResponse(errors=song.errors)

    @method_decorator(permission_required('mainapp.delete_song'))
    def delete(self, request, id): # Ready
        try:
            song=Song.objects.get(pk=id)
        except Song.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist song with {0} id".format(id)],
            )
        if str(song.path):
            os.remove(MEDIA_ROOT+"/songs/"+str(song.path))
        song.delete()
        return WrappedResponse()

class SongFileView(APIView): # Part
    @method_decorator(permission_required('mainapp.see_song_file'))
    def get(self, request, id):
        try:
            song=Song.objects.get(pk=id)
        except Song.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist track with {0} id".format(id)],
            )
        # Converting link to file
        data={'path': reverse('media_song', args=[str(song.path)])}
        return WrappedResponse(data)

class SongPlayedView(APIView): # Part
    @method_decorator(permission_required('mainapp.see_song_file'))
    def post(self, request, id):
        try:
            song=Song.objects.get(pk=id)
        except Song.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist track with {0} id".format(id)],
            )
        # Add check for time delta
        songplayed=SongPlayed(song=song, user=request.session.user)
        songplayed.save()
        return WrappedResponse()

class JinglesView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_jingle'))
    def get(self, request): # Ready
        jingles=Jingle.objects.all()
        
        # Filter
        value=request.query_params.get('title', None)
        if value:
            songs=songs.filter(title__regex=value)
        value=request.query_params.get('author', None)
        if value:
            if type(value) is int:
                jingles=jingles.filter(author__id=value)
            else:
                return WrappedResponse(
                    errors=["Incorrect author id '{0}' value".format(value)],
                )
        value=request.query_params.get('duration', None)
        compare=request.query_params.get('duration_compare', '')
        if value and not value.isdigit():
            return WrappedResponse(errors=["Incorrect 'duration'"])
        if not compare in ('', '__gt', '__lt', '__gte', '__lte'):
            return WrappedResponse(
                errors=["Incorrect 'duration_compare'"],
            )
        if value:
            jingles=jingles.filter(**{'duration'+compare: value})
        order_by=request.query_params.get('order_by', None)
        if order_by:
            jingles=jingles.order_by(order_by)
        
        try:
            page=paginator.paginate_queryset(jingles, request)
        except (DataError, FieldError):
            return WrappedResponse(errors=["Incorrect search data"])
        
        jingles=JingleSerializer(
            page,
            many=True,
        )
        return WrappedResponse(
            paginator.get_paginated_response(jingles.data).data,
        )
    
    @method_decorator(permission_required('mainapp.add_jingle'))
    def post(self, request): # Ready
        data=request.data
        delete=[]
        for key, value in data.items():
            if not value:
                delete.append(key)
        for key in delete:
            del data[key]
        if not 'author' in data:
            data['author']=request.session.user.id
        jingle=JingleSerializer(data=data)
        if jingle.is_valid():
            obj=jingle.save()
            while True:
                try:
                    obj.link=''.join(choice(ascii_letters+digits) for i in range(64))
                    break
                except ValueError:
                    pass
            obj.save()
            return WrappedResponse({
                'link': reverse('api_jingles')+'?link='+obj.link,
                'id': obj.id,
            })
        return WrappedResponse(errors=jingle.errors)
    
    def put(self, request): # Ready
        if not 'link' in request.query_params:
            return WrappedResponse(errors=["Link required"])
        try:
            jingle=Jingle.objects.get(link=request.query_params['link'])
        except Jingle.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist jingle with {0} upload link".format(request.query_params['link'])]
            )
        if not 'file' in request.data:
            return WrappedResponse(errors=["No uploaded song"])
        jinglefile=request.data.get("file")
        if jinglefile.content_type[:5]!='audio':
            return WrappedResponse(errors=["Uploaded file isn't audio"])
        jf=File(jinglefile.temporary_file_path(), easy=True)
        if not jingle.title and 'title' in jf.tags:
            jingle.title=jf.tags['title'][0]
        jf.delete()
        jingle.duration=jf.info.length
        while True:
            jinglefilename=''.join(choice(ascii_letters+digits) for i in range(10))+'.'+jinglefile.content_type[6:]
            fs=FileSystemStorage(location=MEDIA_ROOT+"/jingles")
            if not fs.exists(jinglefilename):
                break
        jingle.path=fs.save(jinglefilename, ContentFile(jinglefile.read()))
        jingle.link=None
        jingle.save()
        return WrappedResponse()

class JinglesUpdateView(APIView): # Ready
    @method_decorator(permission_required('mainapp.update_jingle'))
    def post(self, request):
        if 'author' in request.data:
            try:
                author=User.objects.get(id=request.data['author'])
            except User.DoesNotExist:
                return WrappedResponse(
                    errors=["Doesn't exist user with {0} id".format(request.data['author'])],
                )
        else:
            author=request.session.user
        fs=FileSystemStorage(location=MEDIA_ROOT+"/jingles")
        response={'added': 0, 'deleted': 0}
        for media in fs.listdir('.')[1]:
            try:
                jingle=Jingle.objects.get(path=media)
            except:
                jf=File(MEDIA_ROOT+'/jingles/'+media, easy=True)
                if jf is None:
                    response['deleted']+=1
                    os.remove(MEDIA_ROOT+'/jingles/'+media)
                    continue
                data={}
                if jf.tags:
                    if 'title' in jf.tags:
                        data['title']=jf.tags['title'][0]
                jf.delete()
                data['duration']=jf.info.length
                jf.save()
                while True:
                    name=''.join(choice(ascii_letters+digits) for i in range(10))+os.path.splitext(media)[1]
                    if not fs.exists(name):
                        break
                os.rename(
                    MEDIA_ROOT+'/jingles/'+media,
                    MEDIA_ROOT+'/jingles/'+name,
                )
                response['added']+=1
                data['path']=name
                data['author']=author
                jingle=Jingle(**data)
                jingle.save()
        jingles=Jingle.objects.all()
        for jingle in jingles:
            if not fs.exists(jingle.path):
                response['deleted']+=1
                jingle.delete()
        return WrappedResponse(response)

class JingleView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_jingle'))
    def get(self, request, id): # Ready
        try:
            jingle=Jingle.objects.get(pk=id)
        except Jingle.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist jingle with {0} id".format(id)],
            )
        jingle=JingleSerializer(
            jingle, 
            remove_fields=['path'],
        )
        return WrappedResponse(jingle.data)
    
    @method_decorator(permission_required('mainapp.change_jingle'))
    def post(self, request, id): # Ready
        try:
            jingle=Jingle.objects.get(pk=id)
        except Jingle.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist jingle with {0} id".format(id)],
            )
        if request.session.user.groups.filter(name__in=['user']).exists() and request.session.user!=jingle.author:
            return WrappedResponse(errors=["Access denied"])
        
        data=request.data
        delete=[]
        for key, value in data.items():
            if not value:
                delete.append(key)
        for key in delete:
            del data[key]
        """if 'author' in data:
            if not type(data['author']) is int:
                return WrappedResponse(
                    errors=["Incorrect author '{0}' id".format(data['author'])],
                )
            if request.session.user.groups.filter(name__in=['user']).exists():
                return WrappedResponse(errors=["Access denied"])
            try:
                author=User.objects.get(id=data['author'])
            except User.DoesNotExist:
                return WrappedResponse(
                    errors=["Doesn't exist user with '{0}' id".format(data["author"]),
                )"""
        jingle=JingleSerializer(
            jingle,
            remove_fields=['id', 'duration', 'path', 'author',
                'created_at', 'updated_at'],
            data=data,
        )
        if jingle.is_valid():
            jingle.save()
            return WrappedResponse()
        return WrappedResponse(errors=jingle.errors)

    @method_decorator(permission_required('mainapp.delete_jingle'))
    def delete(self, request, id): # Ready
        try:
            jingle=Jingle.objects.get(pk=id)
        except Jingle.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist jingle with {0} id".format(id)],
            )
        if request.session.user.groups.filter(name__in=['user']).exists and request.session.user!=jingle.author:
            return WrappedResponse(errors=["Access denied"])
        if str(jingle.path):
            os.remove(MEDIA_ROOT+"/jingles/"+str(jingle.path))
        jingle.delete()
        return WrappedResponse()

class JingleFileView(APIView): # Part
    def get(self, request, id):
        try:
            jingle=Jingle.objects.get(pk=id)
        except Jingle.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist jingle with '{0}' id".format(id)],
            )
        if request.session.user.groups.filter(name__in=['user']).exists and request.session.user!=jingle.author:
            return WrappedResponse(errors=["Access denied"])
        # Converting link to file
        data={'path': reverse('media_jingle', args=[str(jingle.path)])}
        return WrappedResponse(data)

class TagsView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_tag'))
    def get(self, request): # Ready
        tags=Tag.objects.all()
        if 'name' in request.query_params:
            tags=tags.filter(name__regex=request.query_params['name'])
        if 'group' in request.query_params:
            tags=tags.filter(group__name__regex=request.query_params['group'])
        if 'order_by' in request.query_params:
            tags=tags.order_by(request.query_params['order_by'])
        
        try:
            page=paginator.paginate_queryset(tags, request)
        except (DataError, FieldError):
            return WrappedResponse(errors=["Incorrect search data"])
        
        tags=TagSerializer(
            page,
            many=True,
        )
        data=paginator.get_paginated_response(tags.data).data
        data['results']=[tag['name'] for tag in data['results']]
        return WrappedResponse(data)
    
    @method_decorator(permission_required('mainapp.add_tag'))
    def post(self, request): # Ready
        data=request.data
        if 'group' in data:
            if data['group']:
                try:
                    group=TagGroup.objects.get(name=data['group'])
                except TagGroup.DoesNotExist:
                    return WrappedResponse(["Doesn't exist tag group with {0} name".format(data['group'])])
                data['group']=group.id
            else:
                data['group']=None
        tag=TagSerializer(data=data)
        if tag.is_valid():
            tag.save()
            return WrappedResponse()
        return WrappedResponse(errors=tag.errors)

class TagView(APIView): # Part
    @method_decorator(permission_required('mainapp.see_tag'))
    def get(self, request, name): # Ready
        try:
            tag=Tag.objects.get(name=name)
        except Tag.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist tag with {0} name".format(name)],
            )
        group=tag.group
        tag=TagSerializer(tag)
        data=tag.data
        if group:
            data['group']=group.name
        return WrappedResponse(data)
    
    @method_decorator(permission_required('mainapp.change_tag'))
    def post(self, request, name): #Ready
        try:
            tag=Tag.objects.get(name=name)
        except Tag.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist tag with {0} name".format(name)],
            )
        data=request.data
        if 'group' in data:
            if data['group']:
                try:
                    taggroup=TagGroup.objects.get(name=data['group'])
                except TagGroup.DoesNotExist:
                    return WrappedResponse(
                        ["Doesn't exist tag group with {0} name".format(data['group'])]
                    )
                data['group']=taggroup.id
            else:
                data['group']=None
        tag=TagSerializer(
            tag,
            remove_fields=['id', 'name', 'created_at', 'updated_at'],
            data=request.data,
        )
        if tag.is_valid():
            tag.save()
            return WrappedResponse()
        return WrappedResponse(errors=tag.errors)
    
    @method_decorator(permission_required('mainapp.delete_tag'))
    def delete(self, request, name): # Part
        try:
            tag=Tag.objects.get(name=name)
        except Tag.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist tag with {0} name".format(name)],
            )
        # Add remove tags from songs and playlists
        tag.delete()
        return WrappedResponse()

class TagGroupsView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_taggroup'))
    def get(self, request): # Ready
        taggroups=TagGroup.objects.all()
        
        # Filter
        if 'name' in request.query_params:
            taggroups=taggroups.filter(name__regex=request.query_params['name'])
        if 'color' in request.query_params:
            taggroups=taggroups.filter(color=request.query_params['color'])
        if 'order_by' in request.query_params:
            taggroups=taggroups.order_by(request.query_params['order_by'])
        
        try:
            page=paginator.paginate_queryset(taggroups, request)
        except (DataError, FieldError):
            return WrappedResponse(errors=["Incorrect search data"])
        
        taggroups=TagGroupSerializer(
            page,
            many=True,
        )
        
        return WrappedResponse(
            paginator.get_paginated_response(taggroups.data).data
        )
        
    @method_decorator(permission_required('mainapp.add_taggroup'))
    def post(self, request): # Ready
        data=request.data
        if 'color' in data and not data['color']:
            del data['color']
        taggroup=TagGroupSerializer(data=data)
        if taggroup.is_valid():
            taggroup.save()
            return WrappedResponse()
        return WrappedResponse(errors=taggroup.errors)

class TagGroupView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_taggroup'))
    def get(self, request, name):
        try:
            taggroup=TagGroup.objects.get(name=name)
        except TagGroup.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist tag group with {0} name".format(name)],
            )
        taggroup=TagGroupSerializer(taggroup)
        return WrappedResponse(taggroup.data)
    
    @method_decorator(permission_required('mainapp.change_taggroup'))
    def post(self, request, name):
        try:
            taggroup=TagGroup.objects.get(name=name)
        except TagGroup.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist tag group with {0} name".format(name)],
            )
        taggroup=TagGroupSerializer(
            taggroup,
            remove_fields=['id', 'created_at', 'updated_at'],
            data=request.data,
        )
        if taggroup.is_valid():
            taggroup.save()
            return WrappedResponse()
        return WrappedResponse(errors=taggroup.errors)
    
    @method_decorator(permission_required('mainapp.delete_taggroup'))
    def delete(self, request, name):
        try:
            taggroup=TagGroup.objects.get(name=name)
        except TagGroup.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist tag group with {0} name".format(name)],
            )
        taggroup.delete()
        return WrappedResponse()

class PlaylistsView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_playlist'))
    def get(self, request): # Ready
        playlists=Playlist.objects.all()
        
        # Filter
        if 'name' in request.query_params:
            playlists=playlists.filter(name__regex=request.query_params['name'])
        try:
            tags=set(json.loads(request.query_params.get('tags', '[]')))
        except JSONDecodeError:
            return WrappedResponse(
                errors=["Incorrect tags"],
            )
        for tag in tags:
            try:
                Tag.objects.get(name=tag)
            except Tag.DoesNotExist:
                return WrappedResponse(
                    errors=["Doesn't exist tag '{0}'".format(tag)],
                )
        compare=request.query_params.get('tags_compare', 'in')
        ids=[]
        if not compare in ('eq', 'in', 'nin'):
            return WrappedResponse(
                errors=["Incorrect 'tags_compare'"],
            )
        if compare=='eq':
            for playlist in playlists:
                if set(json.loads(playlist.tags).keys())!=tags:
                    ids.append(playlist.id)
        else:
            for playlist in playlists:
                if (tags.issubset(set(json.loads(playlist.tags).keys())))==(compare=='nin'):
                    ids.append(playlist.id)
        for x in ids:
            playlists=playlists.exclude(id=x)
        if 'order_by' in request.query_params:
            playlists=playlists.order_by(request.query_params['order_by'])
        
        try:
            page=paginator.paginate_queryset(playlists, request)
        except (DataError, FieldError):
            return WrappedResponse(errors=["Incorrect search data"])
        
        playlists=PlaylistSerializer(
            page,
            many=True,
        )
        data=paginator.get_paginated_response(playlists.data).data
        for playlist in data['results']:
            playlist['tags']=json.loads(playlist['tags'])
        return WrappedResponse(data)
    
    @method_decorator(permission_required('mainapp.add_playlist'))
    def post(self, request): # Ready
        data=request.data
        if 'tags' in data:
            if type(data['tags']) is dict:
                for tag in data['tags'].keys():
                    try:
                        Tag.objects.get(name=tag)
                    except Tag.DoesNotExist:
                        return WrappedResponse(
                            errors=["Doesn't exist tag '{0}'".format(tag)],
                        )
                    if not type(data['tags'][tag]) is int or data['tags'][tag]<=0:
                        return WrappedResponse(
                            errors=["Incorrect '{0}' tag value".format(data['tags'][tag])],
                        )
                data['tags']=json.dumps(data['tags'])
            else:
                return WrappedResponse(
                    errors=["Incorrect 'tags' structure"],
                )
            
        playlist=PlaylistSerializer(
            data=data,
            remove_fields=('id', 'created_at', 'updated_at'),
        )
        if playlist.is_valid():
            playlist.save()
            return WrappedResponse()
        return WrappedResponse(errors=playlist.errors)

class PlaylistView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_playlist'))
    def get(self, request, id): # Ready
        try:
            playlist=Playlist.objects.get(pk=id)
        except Playlist.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist playlist with {0} id".format(id)],
            )
        playlist=PlaylistSerializer(playlist)
        data=playlist.data
        if 'tags' in data:
            data['tags']=json.loads(data['tags'])
        return WrappedResponse(data)
    
    @method_decorator(permission_required('mainapp.change_playlist'))
    def post(self, request, id): # Ready
        try:
            playlist=Playlist.objects.get(pk=id)
        except Playlist.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist playlist with %d id".format(id)],
            )
        data=request.data
        if 'tags' in data:
            if type(data['tags']) is dict:
                for tag in data['tags'].keys():
                    try:
                        Tag.objects.get(name=tag)
                    except Tag.DoesNotExist:
                        return WrappedResponse(
                            errors=["Doesn't exist tag '{0}'".format(tag)],
                        )
                    if not type(data['tags'][tag]) is int or data['tags'][tag]<=0:
                        return WrappedResponse(
                            errors=["Incorrect '{0}' tag value".format(data['tags'][tag])],
                        )
                data['tags']=json.dumps(data['tags'])
            else:
                return WrappedResponse(
                    errors=["Incorrect 'tags' structure"],
                )
            
        playlist=PlaylistSerializer(
            playlist,
            data=data,
        )
        if playlist.is_valid():
            playlist.save()
            return WrappedResponse()
        return WrappedResponse(errors=playlist.errors)

    @method_decorator(permission_required('mainapp.delete_playlist'))
    def delete(self, request, id): # Ready
        try:
            playlist=Playlist.objects.get(pk=id)
        except Playlist.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist playlist with %d id".format(id)],
            )
        
        playlist.delete()
        return WrappedResponse()

class FeedbacksView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_feedback'))
    def get(self, request): # Ready
        feedbacks=Feedback.objects.all()
        value=request.query_params.get('playlist', None)
        if value:
            feedbacks=feedbacks.filter(playlist__name=value)
        value=request.query_params.get('user', None)
        if value:
            if type(value) is int:
                feedbacks=feedbacks.filter(user__id=value)
            else:
                return WrappedResponse(
                    errors=["Incorrect user id '{0}'".format(value)],
                )
        value=request.query_params.get('song', None)
        if value:
            if type(value) is int:
                feedbacks=feedbacks.filter(song__id=value)
            else:
                return WrappedResponse(
                    errors=["Incorrect song id '{0}'".format(value)],
                )
        value=request.query_params.get('mark', None)
        if value:
            if value=='true':
                feedbacks=feedbacks.filter(mark=True)
            elif value=='false':
                feedbacks=feedbacks.filter(mark=False)
            else:
                return WrappedResponse(
                    errors=["Incorrect 'mark' value"],
                )
        value=request.query_params.get('order_by')
        if value:
            feedbacks=feedbacks.order_by(value)
        
        try:
            page=paginator.paginate_queryset(feedbacks, request)
        except (DataError, FieldError):
            return WrappedResponse(errors=["Incorrect search data"])
            
        feedbacks=FeedbackSerializer(
            page,
            many=True,
        )
        return WrappedResponse(
            paginator.get_paginated_response(feedbacks.data).data,
        )
    
    @method_decorator(permission_required('mainapp.add_feedback'))
    def post(self, request): # Ready
        feedback=FeedbackSerializer(data=request.data)
        if feedback.is_valid():
            feedback.save()
            return WrappedResponse()
        return WrappedResponse(errors=feedback.errors)

class FeedbackView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_feedback'))
    def get(self, request, id): # Ready
        try:
            feedback=Feedback.objects.get(id=id)
        except Feedback.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist feedback with {0} id".format(id)],
            )
        
        feedback=FeedbackSerializer(feedback)
        return WrappedResponse(feedback.data)
    
    @method_decorator(permission_required('mainapp.delete_feedback'))
    def delete(self, request, id): # Ready
        try:
            feedback=Feedback.objects.get(id=id)
        except Feedback.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist feedback with {0} id".format(id)],
            )
        
        feedback.delete()
        return WrappedResponse()

class CallbacksView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_callback'))
    def get(self, request): # Ready
        callbacks=Callback.objects.all()
        value=request.query_params.get('phone', None)
        if value:
            callbacks=callbacks.filter(phone__regex=value)
        value=request.query_params.get('email', None)
        if value:
            callbacks=callbacks.filter(email__regex=value)
        value=request.query_params.get('user', None)
        if value:
            if type(value) is int:
                callbacks=callbacks.filter(user__id=value)
            else:
                return WrappedResponse(
                    errors=["Incorrect user id '{0}'".format(value)],
                )
        value=request.query_params.get('message', None)
        if value:
            callbacks=callbacks.filter(message__regex=value)
        value=request.query_params.get('close', None)
        if value:
            if value=='true':
                callbacks=callbacks.filter(close=True)
            elif value=='false':
                callbacks=callbacks.filter(close=False)
            else:
                return WrappedResponse(
                    errors=["Incorrect 'close' value"],
                )
        value=request.query_params.get('order_by')
        if value:
            callbacks=callbacks.order_by(value)
        
        try:
            page=paginator.paginate_queryset(callbacks, request)
        except (DataError, FieldError):
            return WrappedResponse(errors=["Incorrect search data"])
            
        callbacks=CallbackSerializer(
            page,
            many=True,
        )
        return WrappedResponse(
            paginator.get_paginated_response(callbacks.data).data,
        )
    
    @method_decorator(permission_required('mainapp.add_feedback'))
    def post(self, request): # Ready
        data=request.data
        delete=[]
        for key, value in data.items():
            if not value:
                delete.append(key)
        for key in delete:
            del data[key]
        callback=CallbackSerializer(
            data=data,
            remove_fields=['id', 'close', 'created_at', 'updated_at'],
        )
        if callback.is_valid():
            callback.save()
            return WrappedResponse()
        return WrappedResponse(errors=callback.errors)

class CallbackView(APIView): # Ready
    @method_decorator(permission_required('mainapp.see_callback'))
    def get(self, request, id): # Ready
        try:
            callback=Callback.objects.get(id=id)
        except Callback.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist callback with {0} id".format(id)],
            )
        
        callback=CallbackSerializer(callback)
        return WrappedResponse(callback.data)
    
    @method_decorator(permission_required('mainapp.change_callback'))
    def post(self, request, id): # Ready
        try:
            callback=Callback.objects.get(id=id)
        except Callback.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist callback with {0} id".format(id)],
            )
        data=request.data
        delete=[]
        for key, value in data.items():
            if not value:
                delete.append(key)
        for key in delete:
            del data[key]
        callback=CallbackSerializer(
            callback,
            remove_fields=['id'],
            data=data,
        )
        if callback.is_valid():
            callback.save()
            return WrappedResponse()
        return WrappedResponse(errors=callback.errors)
    
    @method_decorator(permission_required('mainapp.delete_callback'))
    def delete(self, request, id): # Ready
        try:
            callback=Callback.objects.get(id=id)
        except Callback.DoesNotExist:
            return WrappedResponse(
                errors=["Doesn't exist feedback with {0} id".format(id)],
            )
        
        callback.delete()
        return WrappedResponse()

class ShuffleView(APIView):
    @method_decorator(permission_required('mainapp.get_shuffle'))
    def get(self, request):
        if not request.session.user.userdata.playlist:
            return WrappedResponse(
                errors=["User haven't attached playlist"],
            )
        songs=Song.objects.all()
        data=[]
        for i in range(min(songs.count(), 3)):
            data.append(songs[randint(0, songs.count()-1)].id)
        return WrappedResponse(data)
