from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.shortcuts import render
from django.views import View
from django.utils.decorators import method_decorator

from hmacauth.hmacauth import permission_required

from mainapp.models import *

from streamingservice.settings import MEDIA_ROOT



class TestView(View):
    def get(self, request):
        return render(request, 'test.html')

class MediaSongView(View): # Part. Convert link
    @method_decorator(permission_required('mainapp.see_song_file'))
    def get(self, request, filename):
        fs=FileSystemStorage(location=MEDIA_ROOT+"/songs")
        if not fs.exists(filename):
            return HttpResponse("fuck off")
        songfile=fs.open(filename)
        response = HttpResponse(songfile.read(), content_type="audio/mp3")
        response['Content-Disposition'] = 'inline; filename=' + filename
        response.success=True
        return response

class MediaJingleView(View): # Part. Convert link
    @method_decorator(permission_required('mainapp.see_jingle_file'))
    def get(self, request, filename):
        fs=FileSystemStorage(location=MEDIA_ROOT+"/jingles")
        if not fs.exists(filename):
            return HttpResponse("fuck off")
        jinglefile=fs.open(filename)
        response = HttpResponse(jinglefile.read(), content_type="audio/mp3")
        response['Content-Disposition'] = 'inline; filename=' + filename
        response.success=True
        return response
