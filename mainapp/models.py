from datetime import datetime

from django.db import models
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.validators import MaxValueValidator, RegexValidator

User._meta.get_field('email')._unique=True

class Common(models.Model):
    # Fields
    created_at=models.DateTimeField(
        auto_now_add=True,
        blank=True,
    )
    updated_at=models.DateTimeField(
        auto_now=True,
        blank=True,
    )
    
    # Meta
    class Meta:
        abstract=True

class Song(Common):
    # Fields
    title=models.CharField(
        max_length=255,
        null=True,
    )
    artist=models.CharField(
        max_length=255,
        null=True,
    )
    album=models.CharField(
        max_length=255,
        null=True,
    )
    release_year=models.CharField(
        max_length=255,
        null=True,
    )
    duration=models.FloatField(null=True)
    file_size=models.PositiveIntegerField(null=True)
    cover_path=models.ImageField(null=True)
    path=models.FileField(null=True)
    tags=models.TextField(
        blank=True,
        default="[]",
    )
    link=models.CharField(
        max_length=64,
        null=True,
    )
    author=models.ForeignKey(
        User,
        blank=True,
    )

class Jingle(Common):
    title=models.CharField(
        max_length=255,
        null=True,
    )
    author=models.ForeignKey(
        User,
        blank=True,
        related_name='author',
    )
    link=models.CharField(
        max_length=64,
        null=True,
    )
    path=models.FileField(null=True)
    duration=models.FloatField(null=True)

class TagGroup(Common):
    # Fields
    name=models.CharField(
        max_length=255,
        blank=True,
        unique=True,
    )
    color=models.PositiveIntegerField(
        validators=[MaxValueValidator(255)],
        null=True,
    )

class Tag(Common):
    # Fields
    name=models.CharField(
        max_length=255,
        blank=True,
        unique=True,
    )
    group=models.ForeignKey(
        TagGroup,
        null=True,
        on_delete=models.SET_NULL,
    )

class Playlist(Common):
    # Fields
    name=models.CharField(
        max_length=255,
        blank=True,
    )
    tags=models.TextField(
        blank=True,
        default="[]",
    )

class Feedback(Common):
    # Fields
    playlist=models.ForeignKey(
        Playlist,
        blank=True,
    )
    user=models.ForeignKey(
        User,
        blank=True,
    )
    song=models.ForeignKey(
        Song,
        blank=True,
    )
    mark=models.BooleanField(blank=True)

class Callback(Common):
    # Fields
    phone=models.CharField(
        max_length=20,
        validators=[RegexValidator(regex=r'^\+?1?\d{9,15}$')],
        null=True,
    )
    email=models.EmailField(null=True)
    user=models.ForeignKey(
        User,
        null=True,
    )
    message=models.TextField(blank=True)
    close=models.BooleanField(
        blank=True,
        default=False,
    )

class Userdata(Common):
    user=models.OneToOneField(
        User,
        unique=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    playlist=models.ForeignKey(
        Playlist,
        null=True,
        on_delete=models.SET_NULL,
    )
    sessions_count=models.PositiveIntegerField(
        blank=True,
        default=1,
    )

class SongPlayed(Common):
    song=models.ForeignKey(
        Song,
        blank=True,
    )
    user=models.ForeignKey(
        User,
        blank=True,
    )
