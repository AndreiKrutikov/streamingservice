from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

class IncorrectPassword(Exception):
    def __init__(self, username):
        super().__init__("Incorrect password for %s username".format(username))

class AuthenticationBackend(ModelBackend):
    def authenticate(self, username, password):
        try:
            user=User.objects.get(username=username)
        except User.DoesNotExist:
            user=User.objects.get(email=username)
        if user.check_password(password):
            return user
        else:
            raise IncorrectPassword(username)

    def get_user(self, id):
        try:
            return User.objects.get(pk=id)
        except User.DoesNotExist:
            return None
